<?php


namespace vr\rbac;


/**
 * Interface IRolesHolderInterface
 * @package vr\rbac
 */
interface IRolesHolderInterface
{
    /**
     * @return array|null
     */
    public function roles(): ?array;
}