<?php


namespace vr\rbac;

/**
 * Class RbacManager
 * @package vr\rbac
 * @deprecated Use BaseRbacManager instead
 */
abstract class RbacManager extends BaseRbacManager
{
}