<?php


namespace vr\rbac;

use Exception;
use RuntimeException;
use Throwable;
use vr\core\ArrayHelper;
use Yii;
use yii\base\Component;
use yii\base\InvalidConfigException;

/**
 * Class RbacManager
 * @package vr\rbac
 */
abstract class BaseRbacManager extends Component
{
    /**
     *
     */
    const ROLE_GUEST = 'guest';

    /**
     * @throws Exception
     */
    public function reload()
    {
        Yii::$app->authManager->removeAll();

        $this->reloadRoles();
        $this->reloadPermissions();
        $this->reassignAll();
    }

    /**
     * @throws Exception
     */
    protected function reloadRoles(): void
    {
        foreach ($this->roles() as $roleName => $children) {
            if (is_numeric($roleName)) {
                $roleName = $children;
                $children = [];
            }

            if (!is_array($children)) {
                $children = [$children];
            }

            if (Yii::$app->authManager->getRole($roleName)) {
                continue;
            }

            $role = Yii::$app->authManager->createRole($roleName);
            if (!$role) {
                throw new RuntimeException("Role $roleName not found");
            }

            Yii::$app->authManager->add($role);

            foreach ($children as $childName) {
                $child = Yii::$app->authManager->getRole($childName);
                Yii::$app->authManager->addChild($role, $child);
            }
        }
    }

    /**
     * @return array
     *
     * You must override this method for proper functioning of this class
     * The method return an associated or indexed array of roles.
     * An indexed item means an independent role, associative - derived roles
     * Some examples
     * public function roles(): array {
     *      return [
     *          self::GUEST,
     *          'employee' => self::GUEST,
     *          'user' => [self::GUEST, 'employee'],
     *          'admin' => ['employee', 'user']
     *      ];
     * }
     *
     */
    public abstract function roles(): array;

    /**
     * @throws Exception
     */
    protected function reloadPermissions()
    {
        foreach ($this->permissions() as $action => $parents) {
            if (Yii::$app->authManager->getPermission($action)) {
                continue;
            }

            $permission = Yii::$app->authManager->createPermission($action);
            Yii::$app->authManager->add($permission);

            if (!is_array($parents)) {
                $parents = [$parents];
            }

            foreach ($parents as $parentName) {
                $roleOrPermission = Yii::$app->authManager->getRole($parentName) ?: Yii::$app->authManager->getPermission($parentName);
                $given = Yii::$app->authManager->getPermissionsByRole($parentName);

                if (!in_array($action, $given)) {
                    Yii::$app->authManager->addChild($roleOrPermission, $permission);
                }
            }
        }
    }

    /**
     * @return array
     *
     * You must override this method for proper functioning of this class
     * The method return an associated array of permissions for each role or sets of roles
     * Each pair means permission (or mask) => array of roles
     * Some examples
     * public function roles(): array {
     *      return [
     *          'app/*' => ['admin'],
     *          'app/* /get' => ['user', 'employee],
     *          'app/account/sign-in' => [self::GUEST],
     *      ];
     * }
     *
     */
    public abstract function permissions(): array;

    /**
     * @throws Exception
     */
    private function reassignAll()
    {
        $manager = Yii::$app->authManager;
        $manager->removeAllAssignments();

        if (!Yii::$app->user->identityClass) {
            throw new InvalidConfigException('IdentityClass must be set up');
        }

        foreach (call_user_func([Yii::$app->user->identityClass, 'find'])->each() as $user) {

            /** @var IRolesHolderInterface $user */
            if (!is_subclass_of($user, IRolesHolderInterface::class)) {
                throw new InvalidConfigException('Identity must implement IRolesHolderInterface');
            }

            foreach ($user->roles() as $title) {
                if ($role = $manager->getRole($title)) {
                    try {
                        $manager->assign($role, ArrayHelper::getValue($user, 'id'));
                    } catch (Throwable $throwable) {
                    }
                }
            }
        }
    }

    /**
     *
     * @param int $userId
     * @throws Exception|Exception
     */
    public function reassign($userId = null)
    {
        $userId = $userId ?: Yii::$app->user->id;

        $manager = Yii::$app->authManager;
        $manager->revokeAll($userId);

        /** @var IRolesHolderInterface $holder */
        $holder = call_user_func([Yii::$app->user->identityClass, 'findOne'], $userId);

        if (!$holder) {
            throw new RuntimeException('It is not allowed to reload roles and permissions to missing users');
        }

        if (!is_subclass_of($holder, IRolesHolderInterface::class)) {
            throw new InvalidConfigException('Identity must implement IRolesHolderInterface');
        }

        foreach ($holder->roles() as $role) {
            $manager->assign($manager->getRole($role), $userId);
        }
    }
}
