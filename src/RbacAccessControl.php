<?php


namespace vr\rbac;


use Yii;
use yii\base\Action;
use yii\base\ActionFilter;
use yii\web\ForbiddenHttpException;

/**
 * Class RbacAccessControl
 * @package vr\rbac
 *
 * Add this to your controller
 *
 * public function behaviors()
 * {
 *      return array_merge(parent::behaviors(), [
 *          'rbac' => [
 *              'class' => RbacAccessControl::class
 *          ]
 *      ]);
 * }
 *
 */
class RbacAccessControl extends ActionFilter
{
    /**
     * @param Action $action
     * @return bool
     * @throws ForbiddenHttpException
     */
    public function beforeAction($action)
    {
        if (!($result = parent::beforeAction($action))) {
            return false;
        }

        if (!Yii::$app->user->isGuest && Yii::$app->request->isPost) {
            $permissions = [
                $action->uniqueId,
                "{$action->controller->module->id}/*/{$action->id}",
                "{$action->controller->module->uniqueId}/*",
                "{$action->controller->uniqueId}/*",
                '*',
            ];

            $allowed = false;

            foreach ($permissions as $permission) {
                if ($allowed |= Yii::$app->user->can($permission)) {
                    break;
                }
            }

            if (!$allowed) {
                throw new ForbiddenHttpException('You are not allowed to access this entry point');
            }
        }

        return true;
    }
}