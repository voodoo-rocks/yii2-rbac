<?php


namespace vr\rbac\commands;


use Exception;
use vr\rbac\RbacManager;
use yii\console\Controller;

/**
 * Class RbacController
 * @package vr\rbac\commands
 * @deprecated
 */
abstract class RbacController extends Controller
{
    /**
     * @throws Exception
     */
    public function actionReload()
    {
        ($this->createRbacManager())->reload();
    }

    /**
     * @return RbacManager
     */
    protected abstract function createRbacManager(): RbacManager;
}